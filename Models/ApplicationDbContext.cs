﻿using Microsoft.EntityFrameworkCore;

namespace E4_Exam.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Custormer> Custormers { get; set;}
        public DbSet<Employees> Employees { get; set;}
        public DbSet<Transactions> Transactions { get; set;}
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<Reports> Reports { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Reports>()
                .HasOne(r => r.Transactions)
                .WithMany(t => t.reports)
                .HasForeignKey(r => r.TransactionalId);
            modelBuilder.Entity<Reports>()
                .HasOne(r => r.Accounts)
                .WithMany(t => t.reports)
                .HasForeignKey(r => r.AccountId);
            modelBuilder.Entity<Reports>()
                .HasOne(r => r.Logs)
                .WithMany(t => t.reports)
                .HasForeignKey(r => r.LogId);

            modelBuilder.Entity<Logs>()
                .HasOne(r => r.Transactions)
                .WithMany(t => t.logs)
                .HasForeignKey(r => r.TransactionalId);

            modelBuilder.Entity<Transactions>()
                .HasOne(r => r.Employees)
                .WithMany(t => t.transactions)
                .HasForeignKey(r => r.EmployeeId);
        }

    }
}
