﻿using System.ComponentModel.DataAnnotations;

namespace E4_Exam.Models
{
    public class Accounts
    {
        [Key]
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public int CustormerId { get; set; }
        public Custormer Custormer { get; set; }
        public ICollection<Reports> reports { get; set; }

    }
}
